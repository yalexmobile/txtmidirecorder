//
//  ViewController.m
//  TXTMIDIRecorder
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "ViewController.h"

#import <MIKMIDI.h>
#import <AVFoundation/AVFoundation.h>

#import "MusicPlayerOfflineRecorder.h"
#import "TXTMIDIPlayer.h"
#import "WaveformDebugView.h"
#import <EZAudio.h>

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface ViewController ()

@property (nonatomic, strong) TXTMIDIPlayer* player;
@property (nonatomic, strong) AVMIDIPlayer* playerMIDI;
@property (weak, nonatomic) IBOutlet WaveformDebugView *waveformView;

@end

@implementation ViewController

#pragma mark - Actions

- (IBAction)playButtonAction:(UIButton *)sender {
    if (sender.selected == NO) {
        sender.selected = YES;
      
        NSString* fileRoot = [[NSBundle mainBundle]
                              pathForResource:@"MarukoClarinetM" ofType:@"txt"];
        NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot
                                                           encoding:NSUTF8StringEncoding error:nil];
        TXTMIDISequence* description = [[TXTMIDISequence alloc] initWithMidiTxt:fileContents];
        _player = [[TXTMIDIPlayer alloc] initWithTXTMIDISequence:description];
        [_player play];
    } else {
        sender.selected = NO;
        [_player stop];
    }
}

- (IBAction)saveToAFileButtonAction:(id)sender {
    NSError *error;
    NSArray *documentsFolders = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [documentsFolders objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Windband_GeneratedAudios"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }
    }
    
    NSArray *txtFilenames = @[
                              @"YouAreTheAppleOfMyEyeFlute",
                              @"RumBahClarinetM",
                              @"MarukoClarinetM",
                              @"cowboyClarinetM",
                              @"BottleneckinClarinetM"
                             ];
    NSArray *soundBanks = @[
                            @"Grand Piano.sf2",
//                            @"Piano Mark.sf2",
//                            @"FreeFont.sf2",
//                            @"Fury.dls"
                           ];
    
    for (NSString* soundBankFilename in soundBanks) {
        NSString *subfolderPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", soundBankFilename.stringByDeletingPathExtension]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:subfolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:subfolderPath withIntermediateDirectories:NO attributes:nil error:&error];
            if (error) {
                NSLog(@"Error: %@", [error localizedDescription]);
            }
        }

        for (NSString* txtFilename in txtFilenames) {
            NSString* fileRoot = [[NSBundle mainBundle]
                                  pathForResource:txtFilename ofType:@"txt"];
            NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot
                                                               encoding:NSUTF8StringEncoding error:nil];
            TXTMIDISequence* description = [[TXTMIDISequence alloc] initWithMidiTxt:fileContents];
            MusicPlayerOfflineRecorder* musicRecorder = [[MusicPlayerOfflineRecorder alloc] initWithTXTMIDISequence:description
                                                                                                  soundBankFilename:soundBankFilename];
            NSString *fileName = [subfolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@_single_outputs.m4a", txtFilename]];
            [musicRecorder renderIntoFile:fileName];
        }
    }
}

- (IBAction)renderIntoTheMemory:(id)sender {
    TICK;
    NSString* fileRoot = [[NSBundle mainBundle]
                          pathForResource:@"YouAreTheAppleOfMyEyeFlute" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot
                                                       encoding:NSUTF8StringEncoding error:nil];
    TXTMIDISequence* description = [[TXTMIDISequence alloc] initWithMidiTxt:fileContents];
    MusicPlayerOfflineRecorder* musicRecorder = [[MusicPlayerOfflineRecorder alloc] initWithTXTMIDISequence:description
                                                                                          soundBankFilename:@"Grand Piano.sf2"];
    NSData* data = [musicRecorder renderIntoTheMemory];
    TOCK;
    
    [_waveformView setWaveformData:data];
}

@end
