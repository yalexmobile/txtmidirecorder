//
//  WaveformDebugView.m
//  TXTMIDIRecorder
//
//  Created by Yaroslav on 8/3/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "WaveformDebugView.h"

@implementation WaveformDebugView

- (void)setWaveformData:(NSData *)waveformData {
    _waveformData = waveformData;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    if (_waveformData) {
        SInt16 *waveform = (SInt16 *)[_waveformData bytes];
        NSUInteger length = [_waveformData length]/sizeof(SInt16);

        Float32 maxValue = 0;
        for (NSUInteger i = 0; i < length; i++) {
            maxValue = MAX(maxValue, waveform[i]);
        }

        CGContextRef context = UIGraphicsGetCurrentContext();
        for (int i = 0; i < length; i++) {
        CGFloat x = rect.size.width*((float)i/length);
        CGFloat y = rect.size.height/2.0f - rect.size.height/2.0f*((Float32)waveform[i]/maxValue);
        CGContextAddEllipseInRect(context,(CGRectMake (x, y, 1.0, 2.0)));
                                  CGContextDrawPath(context, kCGPathFill);
                                  CGContextStrokePath(context);
        }
    }
}

@end
