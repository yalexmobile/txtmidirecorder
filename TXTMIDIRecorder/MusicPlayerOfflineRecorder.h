//
//  MusicPlayerOfflineRecorder.h
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXTMIDISequence.h"

@interface MusicPlayerOfflineRecorder : NSObject

- (instancetype)initWithTXTMIDISequence:(TXTMIDISequence *)TXTMIDISequence soundBankFilename:(NSString *)soundBankFilename;
@property (nonatomic, strong, readonly) TXTMIDISequence* midiSequence;
@property (nonatomic, strong, readonly) NSString* soundBankFilename;

- (void)renderIntoFile:(NSString *)filePath;
- (NSData *)renderIntoTheMemory;

@end
