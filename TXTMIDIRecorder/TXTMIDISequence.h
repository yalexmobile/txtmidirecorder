//
//  TXTMIDIParser.h
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TXTMIDISequence : NSObject

- (instancetype)initWithMidiTxt:(NSString *)midiTxtSource;
@property (nonatomic, strong, readonly) NSString* midiTxtSource;

/* Default is "\t" */
@property (nonatomic, strong) NSString* spaceString;
/* Default is 20 */
@property (nonatomic, assign) UInt8 minVelocity;

- (void)enumerateNotesUsingBlock:(void (^)(UInt8 note, UInt8 velocity, Float64 delay, Float64 duration))block;

@end
