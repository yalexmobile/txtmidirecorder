//
//  TXTMIDIPlayer.m
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "TXTMIDIPlayer.h"
#import <MIKMIDI.h>

@interface TXTMIDIPlayer()

@property (nonatomic, strong) MIKMIDISequencer* sequencer;

@end

@implementation TXTMIDIPlayer

#pragma mark - Initializers

- (instancetype)initWithTXTMIDISequence:(TXTMIDISequence *)midiSequence {
    if (self) {
        _midiSequence = midiSequence;
        _sequencer = [MIKMIDISequencer sequencer];
        MIKMIDISequence* sequence = [[MIKMIDISequence alloc] init];
        NSError *error = nil;
        MIKMIDITrack* track = [sequence addTrackWithError:&error];
        if (error) {
            NSLog(@"Error adding track for sequence.");
        }
        [_midiSequence enumerateNotesUsingBlock:^(UInt8 note, UInt8 velocity, Float64 delay, Float64 duration) {
            [track addEvent:[MIKMIDINoteEvent noteEventWithTimeStamp:delay*2
                                                                note:note
                                                            velocity:velocity
                                                            duration:duration
                                                             channel:0]];
        }];
        MIKMIDISynthesizer* synthesizer = [_sequencer builtinSynthesizerForTrack:track];
        NSURL *soundfont = [[NSBundle mainBundle] URLForResource:@"Grand Piano" withExtension:@"sf2"];
        if (![synthesizer loadSoundfontFromFileAtURL:soundfont error:&error]) {
            NSLog(@"Error loading soundfont for synthesizer. Sound will be degraded. %@", error);
        }
        _sequencer.sequence = sequence;
    }
    return self;
}

#pragma mark - Helpers

- (void)play {
    [_sequencer startPlayback];
}

- (void)stop {
    [_sequencer stop];
}

@end
