
//
//  TXTMIDIParser.m
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "TXTMIDISequence.h"

@interface TXTMIDISequence()

@end

@implementation TXTMIDISequence

#pragma mark - Initializers

- (instancetype)initWithMidiTxt:(NSString *)midiTxtSource {
    self = [super init];
    if (self) {
        _midiTxtSource = midiTxtSource;
        _spaceString = @"\t";
        _minVelocity = 20;
    }
    return self;
}

#pragma mark - Class methods

- (void)enumerateNotesUsingBlock:(void (^)(UInt8 note, UInt8 velocity, Float64 delay, Float64 duration))block {
    NSArray* allLinedStrings = [_midiTxtSource componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* lineStirng in allLinedStrings) {
        NSArray* lineValues = [lineStirng componentsSeparatedByString:@"\t"];
        if (lineValues.count >= 6) {
            UInt8 note = [(NSString *)[lineValues objectAtIndex:0] integerValue];
            UInt8 velocity = [(NSString *)[lineValues objectAtIndex:1] floatValue];
            Float64 delay = [(NSString *)[lineValues objectAtIndex:5] doubleValue]/1000.0f;
          
            if (note >= self.minVelocity && delay > 0.0) {
                if (block) {
                    block(note, velocity, delay, 4.0);
                }
            }
        }
    }
}

@end
