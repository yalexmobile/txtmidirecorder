//
//  TXTMIDIPlayer.h
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXTMIDISequence.h"

@interface TXTMIDIPlayer : NSObject

- (instancetype)initWithTXTMIDISequence:(TXTMIDISequence *)TXTMIDISequence;
@property (nonatomic, strong, readonly) TXTMIDISequence* midiSequence;

- (void)play;
- (void)stop;

@end
