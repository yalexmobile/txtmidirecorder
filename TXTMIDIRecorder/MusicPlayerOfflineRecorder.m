//
//  MusicPlayerOfflineRecorder.m
//  PlayMIDITextPrototype
//
//  Created by Yaroslav on 7/13/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MusicPlayerOfflineRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "TXTMIDISequence.h"

#define kNumberOfSamplers 10

@interface MusicPlayerOfflineRecorder()

@property (nonatomic, assign) AUGraph graph;
@property (nonatomic, assign) AudioUnit ioUnit;
@property (nonatomic, assign) AudioUnit mixerUnit;
@property (nonatomic, assign) AUNode ioNode;
@property (nonatomic, assign) AUNode mixerNode;

@property (nonatomic, strong) NSArray <NSValue*> * samplerUnits;
@property (nonatomic, strong) NSArray <NSNumber*> * samplerNodes;
@property (nonatomic, strong) NSArray <NSValue *> * musicTracks;

@property (nonatomic, assign) MusicSequence musicSequence;
@property (nonatomic, assign) MusicPlayer player;

@property (nonatomic, assign) Float64 sampleRate;
@property (nonatomic, assign) CGFloat duration;

@end

@implementation MusicPlayerOfflineRecorder

#pragma mark - Initializers

- (instancetype)initWithTXTMIDISequence:(TXTMIDISequence *)midiSequence soundBankFilename:(NSString *)soundBankFilename {
    self = [super init];
    if (self) {
        _sampleRate = 44100;
        _midiSequence =  midiSequence;
        _soundBankFilename = soundBankFilename;
        
        [self _setUpAUGraphAndAudioNodes];
        [self _setUpAudioUnits];
        [self _setupSamplers];
        [self _setupMusicPlayer];
    }
    return self;
}

#pragma mark - SetUp

- (void)_setUpAUGraphAndAudioNodes {
    AUGraph graph;
    AUNode ioNode, mixerNode;
    
    AudioComponentDescription cd;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    
    checkError(NewAUGraph(&graph), "Create new AUGraph failed.");
    
    cd.componentType = kAudioUnitType_Mixer;
    cd.componentSubType = kAudioUnitSubType_MultiChannelMixer;
    
    checkError(AUGraphAddNode(graph, &cd, &mixerNode), "Add mixerNode failed.");
    
    cd.componentType = kAudioUnitType_Output;
    cd.componentSubType = kAudioUnitSubType_GenericOutput;
    
    checkError(AUGraphAddNode(graph, &cd, &ioNode), "Add ioNode failed.");
    
    checkError(AUGraphOpen(graph), "Open AUGraph failed.");
    
    checkError(AUGraphConnectNodeInput(graph, mixerNode, 0, ioNode, 0), "Connect mixerNode -> ioNode failed.");
    
    self.graph = graph;
    self.mixerNode = mixerNode;
    self.ioNode = ioNode;
}

- (void)_setUpAudioUnits {
    AudioUnit ioUnit, mixerUnit;
    
    checkError(AUGraphNodeInfo(self.graph, self.mixerNode, 0, &mixerUnit), "Add mixerUnit failed.");
    checkError(AUGraphNodeInfo(self.graph, self.ioNode, 0, &ioUnit), "Add ioUnit failed.");
    
    checkError(AudioUnitInitialize(ioUnit), "Initialize ioUnit failed.");
    
    checkError(AudioUnitSetProperty(ioUnit,
                                    kAudioUnitProperty_SampleRate,
                                    kAudioUnitScope_Output,
                                    0,
                                    &_sampleRate,
                                    sizeof(_sampleRate)), "Set ioUnit sample rate failed.");;
    checkError(AudioUnitSetProperty(mixerUnit,
                                    kAudioUnitProperty_SampleRate,
                                    kAudioUnitScope_Output,
                                    0,
                                    &_sampleRate,
                                    sizeof(_sampleRate)), "Set mixerUnit sample rate failed.");
  
    AudioStreamBasicDescription clientFormat;
    clientFormat.mSampleRate = _sampleRate;
    clientFormat.mFormatID = kAudioFormatLinearPCM;
    clientFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    clientFormat.mChannelsPerFrame = 1;
    clientFormat.mBitsPerChannel = 16;
    clientFormat.mBytesPerFrame = sizeof(SInt16);
    clientFormat.mBytesPerPacket = sizeof(SInt16);
    clientFormat.mFramesPerPacket = 1;
  
    checkError(AudioUnitSetProperty(ioUnit,
                                    kAudioUnitProperty_StreamFormat,
                                    kAudioUnitScope_Output,
                                    0,
                                    &clientFormat,
                                    sizeof(clientFormat)), "Set stream format failed.");
  
    UInt32 busCount = 50;
    AudioUnitSetProperty(mixerUnit,
                         kAudioUnitProperty_ElementCount,
                         kAudioUnitScope_Input,
                         0,
                         &busCount,
                         sizeof(busCount));

    self.mixerUnit = mixerUnit;
    self.ioUnit = ioUnit;
}


- (void)_setupSamplers {
    NSMutableArray* synths = [NSMutableArray array];
    NSMutableArray* samplerNodes = [NSMutableArray array];
    for (int i = 0; i < kNumberOfSamplers; i++) {
        AUNode samplerNode;
        AudioUnit samplerUnit;
        
        AudioComponentDescription cd;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        cd.componentType = kAudioUnitType_MusicDevice;
        cd.componentSubType = kAudioUnitSubType_Sampler;
        
        checkError(AUGraphAddNode(self.graph, &cd, &samplerNode), "Add Sampler node failed.");
        
        checkError(AUGraphConnectNodeInput(self.graph, samplerNode, 0, self.mixerNode, i + 1), "Connect Sampler node to mixer node failed.");
        
        checkError(AUGraphNodeInfo (self.graph, samplerNode, 0, &samplerUnit), "Sampler node to Sampler Untit failed.");
        
        checkError(AudioUnitSetProperty(samplerUnit,
                                        kAudioUnitProperty_SampleRate,
                                        kAudioUnitScope_Output,
                                        0,
                                        &_sampleRate,
                                        sizeof(_sampleRate)), "Setting sample rate for output failed.");
        
        AUGraphInitialize(self.graph);
        
        NSURL *aUrl = [[NSBundle mainBundle] URLForResource:@"Grand Piano" withExtension:@"sf2"];
        
        NSDictionary *typesByFileExtension = @{@"sf2" : @(kInstrumentType_SF2Preset),
                                               @"dls" : @(kInstrumentType_DLSPreset),
                                               @"aupreset" : @(kInstrumentType_AUPreset)};
        AUSamplerInstrumentData instrumentData;
        instrumentData.fileURL  = (__bridge CFURLRef)aUrl;
        instrumentData.instrumentType = [typesByFileExtension[[aUrl pathExtension]] intValue];
        instrumentData.bankMSB  = kAUSampler_DefaultMelodicBankMSB;
        instrumentData.bankLSB  = kAUSampler_DefaultBankLSB;
        instrumentData.presetID = 0;
        
        checkError(AudioUnitSetProperty(samplerUnit,
                                        kAUSamplerProperty_LoadInstrument,
                                        kAudioUnitScope_Global,
                                        0,
                                        &instrumentData,
                                        sizeof(instrumentData)), "Load instrument failed.");
      
        [synths addObject:[NSValue valueWithPointer:samplerUnit]];
        [samplerNodes addObject:@(samplerNode)];
    }
    self.samplerUnits = synths;
    self.samplerNodes = samplerNodes;
}

- (void)_setupMusicPlayer {
    MusicPlayer player;
    checkError(NewMusicPlayer(&player), "Create new music player failed");

    checkError(NewMusicSequence(&_musicSequence), "Create new music sequence failed.");
    checkError(MusicPlayerSetSequence(player, _musicSequence), "Set player sequence failed.");
    checkError(MusicSequenceSetAUGraph(_musicSequence, self.graph), "Set sequence UIGraph failed.");
    
    NSMutableArray* musicTracksMutable = [NSMutableArray new];
    for (int i = 0; i < self.samplerNodes.count; i++) {
        MusicTrack musicTrack;
        checkError(MusicSequenceNewTrack(_musicSequence, &musicTrack), "Create new track failed.");
        checkError(MusicTrackSetDestNode(musicTrack, (int)[[self.samplerNodes objectAtIndex:i] integerValue]), "Set track dest AUNode failed.");
        [musicTracksMutable addObject:[NSValue valueWithPointer:musicTrack]];
    }
    
    __block NSInteger samplerCounter = 0;
    [_midiSequence enumerateNotesUsingBlock:^(UInt8 note, UInt8 velocity, Float64 delay, Float64 duration) {
        MIDINoteMessage msg;
        msg.channel = samplerCounter + 1;
        msg.duration = duration;
        msg.velocity = velocity;
        msg.note = note;
        MusicTrack musicTrack = [[musicTracksMutable objectAtIndex:samplerCounter] pointerValue];
        checkError(MusicTrackNewMIDINoteEvent(musicTrack, delay*2.0, &msg), "MusicTrackNewMIDINoteEvent() failed.");
        samplerCounter = (samplerCounter + 1 == self.samplerNodes.count? 0 : samplerCounter + 1);
    }];

    for (int i = 0; i < self.samplerNodes.count; i++) {
      MusicTrack musicTrack = [[musicTracksMutable objectAtIndex:i] pointerValue];
      MusicTimeStamp musicTrackDuration = 0;
      UInt32 trackDurationPropertyLength = sizeof(musicTrackDuration);
      checkError(MusicTrackGetProperty(musicTrack, kSequenceTrackProperty_TrackLength, &musicTrackDuration, &trackDurationPropertyLength), "Get track duration failed.");
      self.duration = MAX(musicTrackDuration/2.0f, self.duration);
    }
    
    self.musicTracks = [NSArray arrayWithArray:musicTracksMutable];
    self.player = player;
}

#pragma mark - Class Methods

- (NSData *)renderIntoTheMemory {
    checkError(MusicPlayerStop(self.player), "Stop music player failed.");
    
    checkError(MusicPlayerSetTime(self.player, 0), "Set music player time failed.");
    checkError(MusicPlayerStart(self.player), "Start music player failed.");
    
    UInt32 size;
    AudioStreamBasicDescription clientFormat;
    memset (&clientFormat, 0, sizeof(AudioStreamBasicDescription));
    size = sizeof(clientFormat);
    AudioUnitGetProperty (self.ioUnit,
                          kAudioUnitProperty_StreamFormat,
                          kAudioUnitScope_Output,
                          0,
                          &clientFormat,
                          &size);
    
    AudioStreamBasicDescription const *audioDescription = &clientFormat;
    NSTimeInterval duration = _duration;
    NSUInteger lengthInFrames = duration * audioDescription->mSampleRate;
    const NSUInteger kBufferLength = 512;
    AudioBufferList *bufferList = allocateAndInitAudioBufferList(*audioDescription, kBufferLength);
    AudioTimeStamp timeStamp;
    memset (&timeStamp, 0, sizeof(timeStamp));
    timeStamp.mFlags = kAudioTimeStampSampleTimeValid;
    OSStatus status = noErr;
    
    NSMutableData* dataMutable = [NSMutableData new];
    
    for (NSUInteger i = kBufferLength; i < lengthInFrames; i += kBufferLength) {
        status = [self appendBufferList:bufferList intoData:dataMutable bufferLength:kBufferLength timeStamp:&timeStamp];
        if (status != noErr) {
            break;
        }
    }
    if (status == noErr && timeStamp.mSampleTime < lengthInFrames) {
        NSUInteger restBufferLength = (NSUInteger) (lengthInFrames - timeStamp.mSampleTime);
        AudioBufferList *restBufferList = allocateAndInitAudioBufferList(*audioDescription, (int)restBufferLength);
        status = [self appendBufferList:restBufferList intoData:dataMutable bufferLength:restBufferLength timeStamp:&timeStamp];
        freeAudioBufferList(restBufferList);
    }
    freeAudioBufferList(bufferList);
    if (status != noErr)
        NSLog(@"An error has occurred");
    else
        NSLog(@"Finished rendering into the memory");
    
    checkError(MusicPlayerStop(self.player), "Stop music player failed.");
    
    return dataMutable;
}

- (void)renderIntoFile:(NSString *)filePath {
    checkError(MusicPlayerStop(self.player), "Stop music player failed.");

    checkError(MusicPlayerSetTime(self.player, 0), "Set music player time failed.");
    checkError(MusicPlayerStart(self.player), "Start music player failed.");
    
    UInt32 size;
    AudioStreamBasicDescription clientFormat;
    memset (&clientFormat, 0, sizeof(AudioStreamBasicDescription));
    size = sizeof(clientFormat);
    AudioUnitGetProperty (self.ioUnit,
                          kAudioUnitProperty_StreamFormat,
                          kAudioUnitScope_Output, 0,
                          &clientFormat,
                          &size);
    
    AudioStreamBasicDescription const *audioDescription = &clientFormat;
    ExtAudioFileRef audioFile = [self createAndSetupExtAudioFileWithASBD:audioDescription andFilePath:filePath];
    if (!audioFile)
        return;
    NSTimeInterval duration = _duration;
    NSUInteger lengthInFrames = duration * audioDescription->mSampleRate;
    const NSUInteger kBufferLength = 512;
    AudioBufferList *bufferList = allocateAndInitAudioBufferList(*audioDescription, kBufferLength);
    AudioTimeStamp timeStamp;
    memset (&timeStamp, 0, sizeof(timeStamp));
    timeStamp.mFlags = kAudioTimeStampSampleTimeValid;
    OSStatus status = noErr;
    
    for (NSUInteger i = kBufferLength; i < lengthInFrames; i += kBufferLength) {
        status = [self renderToBufferList:bufferList writeToFile:audioFile bufferLength:kBufferLength timeStamp:&timeStamp];
        if (status != noErr) {
            break;
        }
    }
    if (status == noErr && timeStamp.mSampleTime < lengthInFrames) {
        NSUInteger restBufferLength = (NSUInteger) (lengthInFrames - timeStamp.mSampleTime);
        AudioBufferList *restBufferList = allocateAndInitAudioBufferList(*audioDescription, (int)restBufferLength);
        status = [self renderToBufferList:restBufferList
                              writeToFile:audioFile
                             bufferLength:restBufferLength
                                timeStamp:&timeStamp];
        freeAudioBufferList(restBufferList);
    }
    freeAudioBufferList(bufferList);
    ExtAudioFileDispose(audioFile);
    if (status != noErr)
        NSLog(@"An error has occurred");
    else
        NSLog(@"Finished writing to file at path: %@", filePath);
    
    checkError(MusicPlayerStop(self.player), "Stop music player failed.");
}

#pragma mark - Offline Rendering Methods

- (ExtAudioFileRef)createAndSetupExtAudioFileWithASBD:(AudioStreamBasicDescription const *)audioDescription
                                          andFilePath:(NSString *)path {
    AudioStreamBasicDescription destinationFormat;
    memset(&destinationFormat, 0, sizeof(destinationFormat));
    destinationFormat.mChannelsPerFrame = audioDescription->mChannelsPerFrame;
    destinationFormat.mSampleRate = audioDescription->mSampleRate;
    destinationFormat.mFormatID = kAudioFormatMPEG4AAC;
    ExtAudioFileRef audioFile;
    OSStatus status = ExtAudioFileCreateWithURL(
                                                (__bridge CFURLRef) [NSURL fileURLWithPath:path],
                                                kAudioFileM4AType,
                                                &destinationFormat,
                                                NULL,
                                                kAudioFileFlags_EraseFile,
                                                &audioFile
                                                );
    if (status != noErr) {
        NSLog(@"Can not create ext audio file");
        return nil;
    }
    UInt32 codecManufacturer = kAppleSoftwareAudioCodecManufacturer;
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_CodecManufacturer, sizeof(UInt32), &codecManufacturer
                                     );
    status = ExtAudioFileSetProperty(
                                     audioFile, kExtAudioFileProperty_ClientDataFormat, sizeof(AudioStreamBasicDescription), audioDescription
                                     );
    status = ExtAudioFileWriteAsync(audioFile, 0, NULL);
    if (status != noErr) {
        NSLog(@"Can not setup ext audio file");
        return nil;
    }
    return audioFile;
}

- (OSStatus)renderToBufferList:(AudioBufferList *)bufferList
                   writeToFile:(ExtAudioFileRef)audioFile
                  bufferLength:(NSUInteger)bufferLength
                     timeStamp:(AudioTimeStamp *)timeStamp {
    [self clearBufferList:bufferList];
    AudioUnit outputUnit = self.ioUnit;
    
    OSStatus status = AudioUnitRender(outputUnit, 0, timeStamp, 0, (int)bufferLength, bufferList);
    if (status != noErr) {
        NSLog(@"Can not render audio unit");
        return status;
    }
    timeStamp->mSampleTime += bufferLength;
    
    status = ExtAudioFileWrite(audioFile, (int)bufferLength, bufferList);
    if (status != noErr)
        NSLog(@"Can not write audio to file");
    return status;
}

- (OSStatus)appendBufferList:(AudioBufferList *)bufferList
                    intoData:(NSMutableData *)dataMutable
                  bufferLength:(NSUInteger)bufferLength
                     timeStamp:(AudioTimeStamp *)timeStamp {
    [self clearBufferList:bufferList];
    AudioUnit outputUnit = self.ioUnit;
    
    OSStatus status = AudioUnitRender(outputUnit, 0, timeStamp, 0, (int)bufferLength, bufferList);
    if (status != noErr) {
        NSLog(@"Can not render audio unit");
        return status;
    }
    timeStamp->mSampleTime += bufferLength;
    
    SInt16* data = (SInt16 *)bufferList->mBuffers[0].mData;
    SInt16 buf[bufferLength];
    memcpy(buf, data, bufferLength*sizeof(SInt16));
    [dataMutable appendBytes:buf
                      length:bufferList->mBuffers[0].mDataByteSize];
    
    return status;
}

- (void)clearBufferList:(AudioBufferList *)bufferList {
    for (int bufferIndex = 0; bufferIndex < bufferList->mNumberBuffers; bufferIndex++) {
        memset(bufferList->mBuffers[bufferIndex].mData, 0, bufferList->mBuffers[bufferIndex].mDataByteSize);
    }
}

AudioBufferList *allocateAndInitAudioBufferList(AudioStreamBasicDescription audioFormat, int frameCount) {
    int numberOfBuffers = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? audioFormat.mChannelsPerFrame : 1;
    int channelsPerBuffer = audioFormat.mFormatFlags & kAudioFormatFlagIsNonInterleaved ? 1 : audioFormat.mChannelsPerFrame;
    int bytesPerBuffer = audioFormat.mBytesPerFrame * frameCount;
    AudioBufferList *audio = malloc(sizeof(AudioBufferList) + (numberOfBuffers-1)*sizeof(AudioBuffer));
    if ( !audio ) {
        return NULL;
    }
    audio->mNumberBuffers = numberOfBuffers;
    for ( int i=0; i<numberOfBuffers; i++ ) {
        if ( bytesPerBuffer > 0 ) {
            audio->mBuffers[i].mData = calloc(bytesPerBuffer, 1);
            if ( !audio->mBuffers[i].mData ) {
                for ( int j=0; j<i; j++ ) free(audio->mBuffers[j].mData);
                free(audio);
                return NULL;
            }
        } else {
            audio->mBuffers[i].mData = NULL;
        }
        audio->mBuffers[i].mDataByteSize = bytesPerBuffer;
        audio->mBuffers[i].mNumberChannels = channelsPerBuffer;
    }
    return audio;
}

void freeAudioBufferList(AudioBufferList *bufferList ) {
    for ( int i=0; i<bufferList->mNumberBuffers; i++ ) {
        if ( bufferList->mBuffers[i].mData ) free(bufferList->mBuffers[i].mData);
    }
    free(bufferList);
}

#pragma mark - Helpers

static void checkError(OSStatus error, const char *operation) {
    if (error == noErr) return;
    
    char errorString[20];
    
    // See if it appears to be a 4-char-code *(UInt32 *)(errorString + 1) =
    CFSwapInt32HostToBig(error);
    if (isprint(errorString[1]) && isprint(errorString[2]) &&
        isprint(errorString[3]) && isprint(errorString[4])) {
        
        errorString[0] = errorString[5] = '\''; errorString[6] = '\0';
    }
    else {
        
        // No, format it as an integer sprintf(errorString, "%d", (int)error);
        fprintf(stderr, "Error: %s (%s)\n", operation, errorString);
    }
}

#pragma mark - 

- (void)dealloc {
    checkError(MusicPlayerStop(self.player), "Music player stop failed.");
    checkError(DisposeMusicPlayer(self.player), "Dispose player failed.");

    NSArray* tracks = self.musicTracks;
    self.musicTracks = nil;
    for (NSValue* value in tracks) {
        MusicTrack musicTrack = [value pointerValue];
        checkError(MusicSequenceDisposeTrack(_musicSequence, musicTrack), "Dispose music track failed.");
    }
    checkError(DisposeMusicSequence(_musicSequence), "Dispose music sequence failed.");
    
    for (NSValue* value in self.samplerUnits) {
        AudioUnit* audioUnit = [value pointerValue];
        audioUnit = NULL;
    }
    
    checkError(AUGraphStop(self.graph), "Stop grapg failed.");
    checkError(AUGraphUninitialize(self.graph), "Uninitialize failed.");
    checkError(AUGraphClose(self.graph), "Close graph failed.");
    
    self.graph = NULL;
    self.mixerUnit = NULL;
    self.musicSequence = NULL;
}

@end
